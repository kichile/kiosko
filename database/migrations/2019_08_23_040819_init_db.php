<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->boolean('active')->default(false);

            $table->timestamps();
            $table->integer('event_ki_id')->nullable();
        });

        Schema::create('members', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('name', 100)->nullable();
            $table->string('dni', 30)->nullable();
            $table->string('company', 100)->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('email', 50)->nullable();
            $table->boolean('newrecord')->default(true);
            $table->boolean('checkin')->default(false);

            $table->timestamps();
            $table->integer('member_ki_id')->nullable();

            $table->foreign('event_id')->references('id')->on('events');
        });

        Schema::create('backups', function(Blueprint $table) {
            $table->string('import_string');
            $table->integer('event_id');
            $table->increments('member_id');
            $table->string('event_name', 100)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('dni', 30)->nullable();
            $table->string('company', 100)->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('email', 50)->nullable();
            $table->boolean('newrecord')->default(true);
            $table->boolean('checkin')->default(false);
            $table->integer('event_ki_id')->nullable();
            $table->integer('member_ki_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
        Schema::dropIfExists('events');
        Schema::dropIfExists('backups');
    }
}
