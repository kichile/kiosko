<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BatchRegister extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batchs_register', function(Blueprint $table) {
            $table->string('batch_id');
            $table->integer('event_id');
            $table->integer('event_ki_id');
            $table->integer('member_id');
            $table->integer('member_ki_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batchs_register');
    }
}
