<html>
<head>
    <title>Kiosko - CP</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    @if (session()->has('alert'))
    <div class="alert alert-{{ session()->get('alert.tipo') }} mt-3">
        <strong>{{ session()->get('alert.titulo') }}!</strong> {{ session()->get('alert.descripcion') }}
    </div>
    @endif
    <div class="row mt-2">
        <div class="col-6">
            <!--<div class="card border-dark">
                <div class="card-header">
                    Eventos - Importar desde Kiticket.cl
                </div>
                <div class="card-body">
                    <form class="mb-n1" name="evento_importar" action="/admin/event/import" method="POST">
                        @csrf
                        <div class="form-group">
                            <select class="form-control" name="evento_id">
                                @foreach ($events_kiticket as $event_kiticket)
                                    <option value="{{$event_kiticket->id}}">{{$event_kiticket->nombre}}</option>
                                @endforeach
                            </select>
                            <div class="form-check mt-3">
                                <input class="form-check-input" type="checkbox" value="1" name="hacer_backup" checked>
                                <label class="form-check-label">
                                    Realizar un backup de lo que ya existe actualmente en <b>Kiosko</b>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" name="activar_evento" checked>
                                <label class="form-check-label">
                                    Dejar evento <b>Activo</b>
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Importar</button>
                    </form>
                </div>
            </div>-->

            <div class="card border-dark mt-3">
                <div class="card-header">
                    Eventos - Activar
                </div>
                <div class="card-body">
                    <form class="mb-n1" name="evento_exportar" action="/admin/event/activate" method="POST">
                        @csrf
                        <div class="form-group">
                            <select class="form-control" name="event_id">
                                @foreach ($events_kiosko as $event_kiosko)
                                    <option value="{{$event_kiosko->id}}"
                                        {{($event_kiosko->id === $event_active_id) ? 'selected="selected"' : ''}}>
                                        {{$event_kiosko->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Activar</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-6">
            <!--<div class="card border-dark">
                <div class="card-header">
                    Eventos - Exportar a Kiticket.cl
                </div>
                <div class="card-body">
                    <form class="mb-n1" name="evento_exportar" action="/admin/event/export" method="POST">
                        @csrf
                        <div class="form-group">
                            <select class="form-control" name="evento_id">
                                @foreach ($events_kiosko as $event_kiosko)
                                    <option value="{{$event_kiosko->id}}">{{$event_kiosko->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Exportar</button>
                    </form>
                </div>
            </div>-->

            <div class="card border-dark mt-3">
                <div class="card-header">
                    Nuevo Evento
                </div>
                <div class="card-body">
                    <form class="mb-n1" name="evento_exportar" action="/admin/event/create" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre Evento" required="required" />
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="cantidad" placeholder="Cantidad Miembros" required="required" />
                        </div>

                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1" name="label_type">
                                <option value="{{\App\Modules\Label\BasicLabel::NAME}}">Etiqueta - {{\App\Modules\Label\BasicLabel::NAME}}</option>
                                <option value="{{\App\Modules\Label\QRLabel::NAME}}">Etiqueta - {{\App\Modules\Label\QRLabel::NAME}}</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1">RUT / DNI</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1">Nombre Completo</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1">Género</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1">Fecha de Nacimiento</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1" checked>Empresa</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1">Celular</label>
                            </div>

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                                <label class="custom-control-label" for="customCheck1">Email</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary float-right block">Generar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery-3.4.1.slim.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>
