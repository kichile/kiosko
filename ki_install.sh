#!/bin/bash

# Copiar .env
cp .env.example .env

# Eliminar dockers si es que existen
echo "Docker: Eliminando dockers existentes"
docker-compose rm -s -v -f php-kiosko app-kiosko db-kiosko

# Generar containers
echo "Docker: Generar nuevos containers para app"

# Levantar servicios docker
docker-compose up -d app-kiosko

# Instalar dependencias php
docker-compose exec php-kiosko composer install

#
docker-compose stop
docker-compose up -d

# Cache laravel
echo "Artisan: cache clear & build"
docker-compose exec php-kiosko php /code/artisan config:clear
docker-compose exec php-kiosko php /code/artisan config:cache

# Correr migraciones
echo "Artisan: Correr migraciones"
docker-compose exec php-kiosko php /code/artisan migrate

# optimize
docker-compose exec php-kiosko php /code/artisan optimize
