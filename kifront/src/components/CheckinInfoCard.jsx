import React from 'react';

const CheckinInfoCard = props => {
    return (
        <div className="card text-center" style={{lineHeight: '0.9'}}>
            <div className={"card-header alert-" + props.type}>{props.text}</div>
            <div className="card-body" style={{padding:'0.9rem'}}>
                <span className="card-text">{props.quantity}</span>
            </div>
        </div>
    )
}

export default CheckinInfoCard;