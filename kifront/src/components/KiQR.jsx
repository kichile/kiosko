import React from 'react';
import QRCode from 'qrcode.react';

/**
 * 
 * @param {*} props 
 */
const KiQR = (props) =>  {
    /**
     * 
     */
    function stringGenerator() {
        let qrString = 'BEGIN:VCARD\n';
        qrString += 'VERSION:3.0\n';
        qrString += 'N:' + props.person.name + '\n';
        qrString += 'ORG:' + props.person.company + '\n';
        qrString += 'EMAIL;TYPE=INTERNET:' + props.person.email + '\n';
        qrString += 'TEL:' + props.person.phone + '\n';
        qrString += 'URL:http://www.kichile.cl\n';
        qrString += 'END:VCARD';

        return qrString;
    }

    return (
        <QRCode value={stringGenerator()} />
    )
};

export default KiQR;