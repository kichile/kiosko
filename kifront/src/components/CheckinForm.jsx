import React from 'react';
import {Form, Button, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import Input from './form/Input';
import Phone from './form/Phone';

const CheckinForm = ({person, event, clearForm, savePerson}) => {
    const fields = [
        {
            input:"Input", 
            type:"text",
            placeholder: "Nombre Completo",
            attr: "name",
        },
        {
            input:"Input", 
            type:"text",
            placeholder: "Empresa",
            attr: "company",
        },
        {
            input:"Phone",             
            placeholder: "Teléfono",
            attr: "phone",
        },
        {
            input:"Input", 
            type:"text",
            placeholder: "Email",
            attr: "email",
        }
    ];

    const linker = {
        'name': person.name,
        'company': person.company,
        'phone': person.phone,
        'email': person.email,
    };    

    return (        
        <Form>
            {fields.map((field, i) => {
                if (field.input === "Input") {
                    return (<Input type={field.type} placeholder={field.placeholder} attr={field.attr} value={linker[field.attr]} />)
                }
                
                if (field.input === "Phone") {
                    return (<Phone placeholder={field.placeholder} attr={field.attr} value={linker[field.attr]} />)
                }
            })}
                                            
            <Row>
                <Col>
                    <Button outline type="button" color="secondary" block onClick={() => clearForm()}>Limpiar</Button>
                </Col>
                <Col>
                    <Button disabled={(person.name == "")} type="button" color="primary" block onClick={() => savePerson(event.id, person)}>Guardar</Button>
                </Col>
            </Row>            
        </Form>
    )
};

const mapStateToProps = state => {
    return {
        person: state.people.person,
        event: state.people.event,
    }
};

const mapDispatchToProps = (dispatch) => ({    
    clearForm: () => {
        dispatch({type: 'PERSON_CLEAR'});
        window._typeahead.clear();
    },
    savePerson: (event_id, person, checkin = true) => {
        if (person.name.trim() === "" || person.phone.trim() === "" || person.email.trim() === "")
        {
            toast.error("El nombre, celular y email son obligatorios")
            return;
        }

        person.event_id = event_id; // se asocia evento        
        person.checkin = checkin;
        
        if (person.id === null)
        {
            person.print = true;
        }

        fetch("http://localhost:9090/api/member/save", {
                method: 'POST',
                body: JSON.stringify(person),
                headers:{
                  'Content-Type': 'application/json'
                }
              }).then(res => res.json())
              .catch(error => {
                    toast.error('Hay un error en el backend!');
              })
              .then(response => {   
                    if (!response.success)
                    {
                        toast.error(response.message);
                    } else {
                        if (person.id == null) {                                                                                    
                            dispatch({type: 'PERSON_ADD', person: response.data});
                        } else {
                            dispatch({type: 'PERSON_SAVE', person: response.data})                            
                        }

                        dispatch({type: 'PERSON_CLEAR'});
                        window._typeahead.clear();
                    }
              })
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CheckinForm);
