import React, { Component } from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import { connect } from 'react-redux';
import { Badge } from 'reactstrap';

const _renderMenuItemChildren = option => {    
    return (
        <div>
            {option.name}
            <div>
                <small>
                    DNI: {option.dni} 
                    | Empresa: {option.company} {' '} 
                    {(option.checkin) ? <Badge color="success">CHECKIN</Badge> : <Badge color="warning">PENDIENTE</Badge>}
                </small>                
            </div>
        </div>
    )
};

class PeopleFinder extends Component {
    render() {
        return (
            <Typeahead
                {...this.props}
                id="th-people-finder"
                selectHintOnEnter={true}
                labelKey="name"
                placeholder="Escriba el nombre o apellido"
                options={this.props.options}
                renderMenuItemChildren={(option) => _renderMenuItemChildren(option)}
                onChange={(selected) => this.props.onSelect(selected[0])} 
                minLength={3}
                ref={(ref) => window._typeahead = ref}
            />
        )
    }
};

const mapStateToProps = state => {
    return {
        
    }
};

const mapDispatchToProps = dispatch => ({
    onSelect: (person) => {
        dispatch({type: 'PERSON_SELECT', person});
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(PeopleFinder);
