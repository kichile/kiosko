import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Card, CardHeader, CardBody, Badge, Button } from 'reactstrap';
import PeopleFinder from './PeopleFinder';
import LabelPrinterPreview from './LabelPrinterPreview';
import CheckinForm from './CheckinForm';
import { connect } from 'react-redux';
import CheckinInfo from './CheckinInfo';
import QRLectorMode from './QRLectorMode';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';

class App extends Component {
  componentDidMount() {
    fetch("http://localhost:9090/api/event/active")
        .then(res => res.json())
        .then(
            (result) => {
              if (!result.success) {
                toast.error(result.message);
              } else {
                this.props.onLoadData(result.data);
              }
            },
            (error) => {
                toast.error('Hay un error en el backend!!');
            }
        )
  }

  render() {
    return (
      <div className="App">        
        <Container>
          <h4>Evento {this.props.event.name}</h4>
          <hr style={{margin:'0'}}/>
          <CheckinInfo />
          <Row className="mt-4">
            <Col className="col-6">
              <Card>
                <CardHeader>Buscar Persona</CardHeader>
                <CardBody>
                  <PeopleFinder options={this.props.options} />
                </CardBody>
              </Card>

              <Card className="mt-4">
              {(this.props.person.id != null) ? (
                (this.props.person.checkin) ? 
                  <CardHeader className="alert-success">Actualizar Datos <Badge color="success" pill>CHECKIN OK</Badge></CardHeader>
                  :
                  <CardHeader className="alert-warning">Actualizar Datos <Badge color="warning" pill>PENDIENTE</Badge></CardHeader>
              ) : (
                <CardHeader>Agregar Persona</CardHeader>
              )}
                <CardBody>
                  <CheckinForm />
                </CardBody>
              </Card>
            </Col>

            <Col className="col-6">
              <Card>
                <CardHeader>Preview Etiqueta</CardHeader>
                <CardBody>
                  <LabelPrinterPreview />
                </CardBody>
              </Card>

              <Card className="mt-4">
                <CardHeader>Opciones</CardHeader>
                <CardBody>
                  <CardBody className='text-center'>
                    <Row>
                      <Col>
                        <QRLectorMode />
                      </Col>
                      <Col>
                        <Button 
                          color={(this.props.person.checkin) ? 'danger' : 'primary'}
                          block 
                          onClick={() => this.props.setCheckin(this.props.person)} 
                          disabled={(this.props.person.id == null)}
                        >
                          {(this.props.person.checkin) ? 'CHECKOUT' : 'CHECKIN'}
                      </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </CardBody>
              </Card>
            </Col>          
          </Row>
          <hr style={{marginBottom:'0'}} />
          <div className="text-center">
            <span style={{fontSize: '10pt'}}>2019 © Ki Chile - Kiosko {this.props.version}</span>
          </div>
        </Container>
        <ToastContainer autoClose={3000} />        
      </div>
    );
  }
};

const mapStateToProps = state => {
  return {
      person: state.people.person,
      options: state.people.options,
      event: state.people.event,
      version: state.people.version,
  }
};

const mapDispatchToProps = dispatch => ({
  setCheckin: (person) => {
    person.checkin = !person.checkin;

    if (person.checkin)
    {
      person.print = true;
    }

    fetch("http://localhost:9090/api/member/save", {
      method: 'POST',
      body: JSON.stringify(person),
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .catch(error => {
          toast.error('Hay un error en el backend!');
    })
    .then(response => {   
        if (!response.success)
        {
            toast.error(response.message);
        } else {
          dispatch({type: 'PERSON_SAVE', person: response.data})
        }
    })
  },
  onLoadData: (data) => dispatch({type: 'LOAD_DATA', data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);