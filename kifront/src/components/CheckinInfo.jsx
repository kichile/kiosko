import React from 'react';
import CheckinInfoCard from './CheckinInfoCard';
import { connect } from 'react-redux';
import _ from 'lodash';

const CheckinInfo = ({checkins, pending, newrecords}) => {
    return (
        <div className="row mt-2">
            <div className="col-md-4">
                <CheckinInfoCard type="success" text="Check-ins" quantity={checkins} />
            </div>
            <div className="col-md-4">
                <CheckinInfoCard type="warning" text="Pendientes" quantity={pending} />
            </div>
            <div className="col-md-4">
                <CheckinInfoCard type="primary" text="Nuevos" quantity={newrecords} />
            </div>
        </div>
    );
}

const mapStateToProps = state => {    
    return {
        checkins: _.sumBy(state.people.options, member => member.checkin),
        pending: _.sumBy(state.people.options, member => !member.checkin),
        newrecords: _.sumBy(state.people.options, member => member.newrecord),
    }
}

export default connect(mapStateToProps)(CheckinInfo);