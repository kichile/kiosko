import React from 'react';
import { FormGroup, Input as InputBase } from 'reactstrap';
import { connect } from 'react-redux';

const Input = props => {        
    return (        
        <FormGroup>
            <InputBase type={props.type} placeholder={props.placeholder} value={props.value} onChange={(e) => props.setData(props.attr, e.target.value)} />
        </FormGroup>                        
    );
};

const mapStateToProps = state => {
    return {
        person: state.people.person,        
    }
};


const mapDispatchToProps = (dispatch) => ({
    setData: (key, value) => dispatch({type: 'PERSON_SET_DATA', key, value})
});

export default connect(mapStateToProps, mapDispatchToProps)(Input);