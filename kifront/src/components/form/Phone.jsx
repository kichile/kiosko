import React from 'react';
import { FormGroup } from 'reactstrap';
import { connect } from 'react-redux';
import ReactPhoneInput from 'react-phone-input-2';

const Phone = props => {        
    return (        
        <FormGroup>
                <ReactPhoneInput 
                    style={{width: '100%'}}
                    placeholder={props.placeholder}
                    defaultCountry={'cl'} 
                    value={ props.value } 
                    onChange={ phone => props.setData(props.attr, phone)} 
                    masks={{'cl': '+.. (.) .... ....', 'at': '+...........'}}
                />            
            </FormGroup>                    
    );
};

const mapStateToProps = state => {
    return {
        person: state.people.person,        
    }
};


const mapDispatchToProps = (dispatch) => ({
    setData: (key, value) => dispatch({type: 'PERSON_SET_DATA', key, value})
});

export default connect(mapStateToProps, mapDispatchToProps)(Phone);