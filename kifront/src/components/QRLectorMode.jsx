import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { 
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    Form,
    FormGroup,
    Input
} from 'reactstrap';
import { toast } from 'react-toastify';

class QRLectorMode extends React.Component {

  render() {
    return (
      <div>
        <Button color="dark" onClick={this.props.toggle} block>MODO PISTOLA</Button>
        <Modal isOpen={this.props.activated} toggle={this.props.toggle} autoFocus={false} centered>
          <ModalHeader>MODO PISTOLA</ModalHeader>
          <ModalBody>
            <Form>
                <FormGroup>
                <Input 
                  autoFocus={true}
                  type="text" 
                  placeholder="Utiliza el lector QR para hacer el Check-in" 
                  onChange={(e) => this.props.handleChange(e)}
                  onKeyDown={(e) => this.props.handleKeyDown(e, this.props.options)}
                  value={this.props.value}
                />
                </FormGroup>
            </Form>
            
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    options: state.people.options, //TODO: cambiar! muy mal
    activated: state.people.scannerMode.activated,
    value: state.people.scannerMode.value,
  }
};

const mapDispatchToProps = dispatch => ({
  toggle: () => {
    dispatch({type: 'TOGGLE_SCANNER_MODE'});
  },

  handleChange(e) {
    dispatch({type: 'SCANNER_SET_VALUE', value: e.target.value})
  },

  handleKeyDown(e, options) {
    
    if (e.keyCode === 13) {
      e.preventDefault();
      let person = _.find(options, function(o) { return o.member_ki_id == e.target.value});

      if (person === undefined) {        
        toast.error("El código no se encuentra registrado!");
        dispatch({type: 'SCANNER_CLEAR'});
        return;
      }

      if (person.checkin)
      {        
        dispatch({type: 'PERSON_SELECT', person});
        dispatch({type: 'SCANNER_CLEAR'});
        toast.error("Esta persona ya fue ingresada al evento previamente"); 
        return;             
      }      

      person.print = true;
      person.checkin = !person.checkin;      

      fetch("http://localhost:9090/api/member/save", {
        method: 'POST',
        body: JSON.stringify(person),
        headers:{
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
      .catch(error => {
            toast.error('Hay un error en el backend!');
      })
      .then(response => {   
            if (!response.success)
            {
                toast.error(response.message);
            } else {
              dispatch({type: 'SCANNER_CHECKIN', person: response.data})
            }
      })
    }
  }

});

export default connect(mapStateToProps, mapDispatchToProps) (QRLectorMode);