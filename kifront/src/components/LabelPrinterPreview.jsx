import React from 'react';
import { Card, Button, Media } from 'reactstrap';
import { connect } from 'react-redux';

/**
 *
 * @param person
 * @returns {*}
 * @constructor
 */
const LabelPrinterPreview = ({person, event}) =>  {
    return (        
        <Card className="text-center border-dark">
            <Media width="100%" src={`http://localhost:9090/labelPreview?name=${person.name}&company=${person.company}&email=${person.email}&phone=${person.phone}&event_id=${event.id}`} /> 
        </Card>                    
    )
};

/**
 *
 * @param state
 * @returns {{person: *}}
 */
const mapStateToProps = state => {
    return {
        person: state.people.person,
        event: state.people.event,
    }
};

/**
 *
 */
export default connect(mapStateToProps)(LabelPrinterPreview);
