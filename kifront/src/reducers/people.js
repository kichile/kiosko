import _ from 'lodash';
import { toast } from 'react-toastify';

//TODO: Refactorizar todo este reducer -_-
/**
 *
 * @type {{options: *[], selected: {name: string, company: string, id: string, dni: string, email: string}}}
 */
const defaultState = {
    options: [],
    event: {
        id: null,
        name: '',
    },
    person: {
        id: null,
        event_id: null,
        name: '',
        company: '',
        phone: '',
        email: '',
        newrecord: true,
        checkin: false,
        member_ki_id: null,
    },
    scannerMode: {
        activated: false,
        value: '',
    },
    version: '',
};

export default (state = defaultState, action) => {

    switch (action.type) {

        case 'PERSON_SELECT':
            var person = (action.person === undefined) ? defaultState.person : action.person; 
            person.phone = String(person.phone) 
        
            return {
                ...state,
                person: person,
            };
        case 'PERSON_SET_DATA':
            if (action.key === 'phone') {
                action.value = action.value.replace(/[\s)(]/g,'');
            } 

            return {
                ...state,
                person: {
                    ...state.person,
                    [action.key]: action.value
                },
            }
        case 'LOAD_DATA':
            return {
                ...state,
                event: action.data.event,
                options: action.data.members,
                version: action.data.version,
                toast: toast.success("Evento cargado con éxito!!"),
            }
        
        case 'PERSON_CLEAR':
            return {
                ...state,
                person: defaultState.person,
            } 
        case 'PERSON_ADD':
            return {
                ...state,
                options: [
                    ...state.options,
                    action.person
                ],
                person: action.person,                
                alert: toast.success("Datos guardados!!")
            }
        case 'PERSON_SAVE':        
            return {
                ...state,
                options: state.options.map((item, index) => {
                    if(item.id === action.person.id) {
                        return action.person;
                    }

                    return item;
                }),
                person: action.person,
                alert: (action.person.checkin) ? toast.success("CHECKIN realizado!") : toast.warning("CHECKOUT realizado!"),
            }
        case 'TOGGLE_SCANNER_MODE':
            return {
                ...state,
                scannerMode: {
                    ...state.scannerMode,
                    activated: !state.scannerMode.activated,
                }
            }
        case 'SCANNER_SET_VALUE':
            return {
                ...state,
                scannerMode: {
                    ...state.scannerMode,
                    value: action.value,
                }
            }
        case 'SCANNER_CHECKIN':
            return {
                ...state,
                scannerMode: {
                    ...state.scannerMode,
                    value: '',
                },
                alert: (action.person.checkin) ? toast.success("CHECKIN realizado!") : toast.warning("CHECKOUT realizado!"),
                person: action.person,
            }
        case 'SCANNER_CLEAR':
            return {
                ...state,
                scannerMode: {
                    ...state.scannerMode,
                    value: '',
                },
            }
        default:
            return state;
    }
}