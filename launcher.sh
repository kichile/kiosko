#!/bin/bash

BASEDIR=$(dirname $0)
cd ${BASEDIR}

docker-compose up -d

# Cache laravel
echo "Artisan: cache clear & build"
docker-compose exec php-kiosko php /code/artisan config:clear
docker-compose exec php-kiosko php /code/artisan config:cache

# optimize
docker-compose exec php-kiosko php /code/artisan optimize

# Correr web react
npm start