<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    /**
     * @var string
     */
    protected $table = 'members';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'event_id', 'name', 'dni', 'company', 'phone', 'email', 'newrecord', 'checkin', 'member_ki_id', 'gender', 'birth_date', 'first_checkin',
    ];
}
