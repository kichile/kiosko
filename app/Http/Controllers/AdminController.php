<?php

namespace App\Http\Controllers;

use App\Modules\Event;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $module = null;

    public function __construct()
    {
        $this->module = new Event();
    }

    public function index(Request $request)
    {
        //$events_kiticket = $this->module->getEventsFromKiticket();
        $events = $this->module->getEvents();
        $activeEvent = $this->module->getActiveEvent();

        return view('cpanel.index', [
            'events_kiticket'  => [],//(is_null($events_kiticket) ? [] : $events_kiticket),
            'events_kiosko'    => (is_null($events) ? [] : $events),
            'event_active_id'  => (is_null($activeEvent) ? '' : $activeEvent->id),
        ]);
    }

    public function importEvent(Request $request)
    {
        $evento_id = $request->get('evento_id');
        $hacer_backup = $request->has('hacer_backup');
        $activar_evento = $request->has('activar_evento');
        $resultado = $this->module->importar($evento_id, $hacer_backup, $activar_evento);
        return redirect('admin')->with(['alert' => $resultado]);
    }

    public function exportEvent(Request $request)
    {
        $evento_id = $request->get('evento_id');
        $resultado = $this->module->exportar($evento_id);
        return redirect('admin')->with(['alert' => $resultado]);
    }

    public function activeEvent(Request $request)
    {
        $event_id = $request->get('event_id');
        $result = $this->module->activate($event_id);
        return redirect('admin')->with(
            ['alert' => [
                'tipo' => 'success',
                'titulo' => 'Success',
                'descripcion' => "El evento ha sido activo. Revisar Kiosko.",
            ]
        ]);
    }

    public function createEvent(Request $request)
    {
        $result = $this->module->create(
            $request->get('nombre'),
            $request->get('cantidad'),
            $request->get('label_type')
        );

        return redirect('admin')->with([
            'alert' => [
                'tipo' => 'success',
                'titulo' => 'Success',
                'descripcion' => "El evento ha sido creado exitosamente",
            ]
        ]);
    }

}
