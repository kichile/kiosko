<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Member;
use App\Modules\Label\BasicLabel;
use App\Modules\Label\QRLabel;
use Illuminate\Http\Request;

/**
 * Class MemberController
 * @package App\Http\Controllers
 */
class MemberController extends BaseController
{


    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $member = Member::updateOrCreate(
            ['id' => $request->get('id')],
            [
                'event_id'      => $request->get('event_id'),
                'name'          => $request->get('name'),
                'company'       => $request->get('company'),
                'dni'           => (is_null($request->get('dni')) ? 'N/D' : $request->get('dni')),
                'phone'         => $request->get('phone'),
                'email'         => $request->get('email'),
                'checkin'       => $request->get('checkin'),
                'newrecord'     => $request->get('newrecord'),
            ]
        );

        if ($request->has('print'))
        {
            if ($request->get('print'))
            {
                $event = Event::find($request->input('event_id'));
                $labelMaker = $this->getLabelMaker($event);

                $labelMaker->make($member);
                $labelMaker->print($member);
            }
        }

        if (!$member->first_checkin and $member->checkin)
        {
            $member->first_checkin = true;
            $member->save();
        }

        return $this->sendResponse($member);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function labelPreview(Request $request)
    {
        $event = Event::find($request->input('event_id'));
        $member = Member::firstOrNew(
            ['id' => $request->get('id')],
            [
                'name'          => $request->input('name', ''),
                'company'       => $request->input('company', ''),
                'phone'         => $request->input('phone'),
                'email'         => $request->input('email'),
            ]
        );

        $labelMaker = $this->getLabelMaker($event);
        $img = $labelMaker->make($member);
        return $img->response('png', 50);
    }

    private function getLabelMaker(Event $event)
    {
        $label_type = (is_null($event) ? '' : $event->label_type );

        switch ($label_type) {
            case QRLabel::NAME:
                $labelMaker = new QRLabel;
                break;
            default:
                $labelMaker = new BasicLabel;
        }

        return $labelMaker;
    }


}
