<?php

namespace App\Http\Controllers;

use App\Modules\Event;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

class EventController extends BaseController
{
    public function activeEvent(Request $request)
    {
        $event = Event::getActiveEvent();

        if (is_null($event))
        {
            return $this->sendErrorResponse('No hay eventos activos!!');
        }

        $members = Event::getMembersByEventId($event->id);
        $process = new Process('git describe --always --tags');
        $process->run();

        if ($process->isSuccessful())
        {
            $version = $process->getOutput();
        } else {
            $version = 'Unknown';
        }

        return $this->sendResponse([
            'event' => $event,
            'members' => $members,
            'version' => $version,
        ]);
    }
}
