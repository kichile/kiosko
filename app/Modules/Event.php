<?php

namespace App\Modules;

use App\Models\Member;
use App\Modules\API\KiticketAPI;
use Carbon\Carbon;
use Faker\Factory as Faker;


class Event
{

    /**
     * @return array
     */
    public function getEvents()
    {
        return \App\Models\Event::all();
    }

    /**
     * @return mixed
     */
    public static function getActiveEvent()
    {
        return \App\Models\Event::where('active', 1)->first();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getEventsFromKiticket() : array
    {
        $events = KiticketAPI::events();
        return is_array($events) ? $events : [];
    }

    public function crearEvento(int $id, string $nombre, bool $activar_evento) : \App\Models\Event
    {
        $evento = \App\Models\Event::firstOrCreate([
            'id' => $id,
        ]);

        if ($activar_evento)
        {
            \DB::table('eventos')->update(['activo' => false]);
        }

        $evento->activo = $activar_evento;
        $evento->nombre = $nombre;
        $evento->save();

        return $evento;
    }

    public function backup_miembros(int $evento_id)
    {
        $backup = Member::where('evento_id', $evento_id)->get()->toArray();
        $import_string = Carbon::now()->timestamp;

        foreach($backup as $key => &$miembro)
        {
            $miembro['import_string'] = $import_string;
        }

        if (count($backup) > 0)
        {
            \DB::table('backup_importacion')->insert($backup);
        }
    }

    public function importar(int $evento_id, bool $hacer_backup, bool $activar_evento)
    {
        try {
            $respose_evento = KiticketAPI::eventos($evento_id);
            $response_miembros = KiticketAPI::miembros($evento_id);
        } catch (\Exception $e) {
            return [
                'tipo' => 'error',
                'titulo' => 'error',
                'descripcion' => $e->getMessage(),
            ];
        }

        $this->crearEvento($respose_evento->id, $respose_evento->nombre, $activar_evento);

        $miembros = [];

        foreach($response_miembros as $miembro)
        {
            $miembros[] = [
                'id' => $miembro->miembro_kiticket_id,
                'evento_id' => $respose_evento->id,
                'tipo_documento_id' => $miembro->tipo_documento_id,
                'dni' => $miembro->dni,
                'nombre' => $miembro->nombre,
                'apellido_paterno' => $miembro->apellido_paterno,
                'apellido_materno' => $miembro->apellido_materno,
                'cargo' => $miembro->cargo,
                'empresa' => $miembro->empresa,
                'celular' => $miembro->celular,
                'email' => $miembro->email,
            ];
        }

        if ($hacer_backup)
        {
            $this->backup_miembros($evento_id);
        }

        Member::where('evento_id', $respose_evento->id)->delete();

        try {
            Member::insert($miembros);
        } catch (\Exception $e) {
            return [
                'tipo' => 'error',
                'titulo' => 'error',
                'descripcion' => $e->getMessage(),
            ];
        }

        return [
            'tipo' => 'success',
            'titulo' => 'Success',
            'descripcion' => 'La acción se ha realizado. Revisar pagina de checkin.',
        ];
    }

    public function exportar(int $evento_id)
    {
        $miembros = Member::where('evento_id', $evento_id)->get();

        if ($miembros->count() == 0)
        {
            return [
                'tipo' => 'warning',
                'titulo' => 'Warning',
                'descripcion' => 'El evento no tiene miembros asociados!',
            ];
        }

        $data = KiticketAPI::exportar(['data' => $miembros->toJSON()]);

        \DB::table('batch_register')->insert([
            'batch_id' => $data->batch_id,
            'evento_id' => $evento_id
        ]);

        return [
            'tipo' => 'success',
            'titulo' => 'Success',
            'descripcion' => "Los datos del evento se han almacenado en kiticket.cl con batch {$data->batch_id}",
        ];
    }

    public function activate(int $event_id)
    {
        \DB::table('events')->update(['active' => false]);
        $event = \App\Models\Event::find($event_id);
        $event->active = 1;
        $event->save();

        return true;
    }

    public function create(string $name, int $totalMembers, string $label_type)
    {
        $event = \App\Models\Event::create([
            'name' => $name,
            'active' => !$this->_areThereActiveEvents(),
            'label_type' => $label_type,
        ]);

        if ($totalMembers > 0)
        {
            foreach (range(1, $totalMembers) as $i)
            {
                $faker = Faker::create();

                $genders = ['MASCULINO', 'FEMENINO'];

                Member::create([
                    'event_id'      => $event->id,
                    'name'          => $faker->name,
                    'dni'           => "{$faker->numberBetween(40000000,90000000)}-{$faker->numberBetween(0,9)}",
                    'company'       => $faker->company,
                    'phone'         => $faker->numberBetween(40000000,90000000),
                    'email'         => $faker->email,
                    'gender'        => $genders[rand(0,1)],
                    'birth_date'    => $faker->dateTimeThisCentury(),
                    'member_ki_id'  => $i,
                ]);
            }
        }

        $this->activate($event->id);

        return true; // TODO
    }

    private function _areThereActiveEvents()
    {
        $event = self::getActiveEvent();
        return (!is_null($event));
    }

    public static function getMembersByEventId(int $eventId)
    {
        return Member
                ::where('event_id', $eventId)
                ->select('id', 'event_id', 'name', 'dni', 'company', 'phone', 'email', 'newrecord', 'checkin', 'member_ki_id')
                ->get();
    }
}
