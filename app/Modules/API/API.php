<?php


namespace App\Modules\API;

use GuzzleHttp;
use Exception;

class API
{
    public static function request($uri, $method = 'GET', $params = null)
    {
        $service = config('services.kiticket');

        if (empty($service['domain']))
        {
            throw new Exception("API no tiene dominio definido!");
        }

        $client = new GuzzleHttp\Client(['base_uri' => $service['domain']]);
        $response = $client->request($method, $uri, ['json' => $params]);
        $content = json_decode($response->getBody()->getContents());

        if (!$content->success)
        {
            throw new Exception("Algo anda mal con la API. Revisar!");
        }

        return $content->data;
    }
}
