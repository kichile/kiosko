<?php


namespace App\Modules\API;

use App\Modules\Event;
use Exception;

class KiticketAPI extends API
{
    /**
     * @param int|null $id
     * @return mixed
     * @throws Exception
     */
    public static function events(int $id = null)
    {
        return self::request('evento' . ((is_null($id)) ? '' : "/{$id}"));
    }

    /**
     * @param int $evento_id
     * @return mixed
     * @throws Exception
     */
    public static function members(int $event_id)
    {
        return self::request("evento/{$event_id}/miembros");
    }

    /**
     * @param $params
     * @return mixed
     * @throws Exception
     */
    public static function export($params)
    {
        return self::request("kiosko", 'POST', $params);
    }

}
