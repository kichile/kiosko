<?php


namespace App\Modules\Label;

use App\Models\Member;
use Intervention\Image\Facades\Image;
use Symfony\Component\Process\Process;

/**
 * Class LabelGenerator
 * @package App\Modules\Label
 */
abstract class LabelGenerator
{
    const CANVAS_HEIGHT = 306;
    const CANVAS_WIDTH = 991;
    const CANVAS_BACKGROUND_COLOR = '#ffffff';

    protected $canvas;

    /**
     * @param Member $member
     */
    abstract function make(Member $member);

    /**
     * LabelGenerator constructor.
     */
    public function __construct()
    {
        $this->canvas = Image::canvas(self::CANVAS_WIDTH, self::CANVAS_HEIGHT, self::CANVAS_BACKGROUND_COLOR);
    }

    /**
     * @param Member $member
     */
    public function save(Member $member) : void
    {
        $this->canvas->save(storage_path("/app/label_userid_{$member->id}.png"), 100, 'png');
    }

    /**
     * @param int $id
     */
    public function print(Member $member)
    {
        $process = new Process("bash /code/print.sh {$member->id}");
        $process->run();
    }

    /**
     * @param string $name
     */
    protected function putName(string $name) : void
    {
        list($firstLine, $secondLine) = $this->decomposeNameInLines($name);
        $fontSize = $this->calculateNameFontSize($firstLine, $secondLine, static::MAX_NAME_FONT_SIZE);

        $this->canvas->text($firstLine, static::TEXT_HORIZONTAL_CENTER_ALIGN, 100, function($font) use ($fontSize) {
            $font->file(public_path('fonts/Roboto-Bold.ttf'));
            $font->size($fontSize);
            $font->color('#000000');
            $font->align('center');
        });

        $secondLinePositionY = static::NAME_FIRST_LINE_POSITION_Y + $fontSize;

        $this->canvas->text($secondLine, static::TEXT_HORIZONTAL_CENTER_ALIGN, $secondLinePositionY, function($font) use ($fontSize) {
            $font->file(public_path('fonts/Roboto-Bold.ttf'));
            $font->size($fontSize);
            $font->color('#000000');
            $font->align('center');
        });
    }

    /**
     * @param string $company
     */
    protected function putCompany(string $company) : void
    {
        $fontSize = $this->calculateCompanyFontSize($company, static::MAX_COMPANY_FONT_SIZE);
        $this->canvas->text(ucwords($company), static::TEXT_HORIZONTAL_CENTER_ALIGN, static::COMPANY_POSITION_Y, function($font) use ($fontSize) {
            $font->file(public_path('fonts/Roboto-Regular.ttf'));
            $font->size($fontSize);
            $font->color('#000000');
            $font->align('center');
        });
    }

    /**
     * @param string $name
     * @return array
     */
    protected function decomposeNameInLines(string $name) : array
    {
        if (empty($name))
        {
            return ['', ''];
        }

        $name = explode(" ", mb_strtoupper($name));

        switch (count($name)) {
            case 1:
                $firstLine = $name[0];
                $secondLine = "";
                break;
            case 2:
                $firstLine = $name[0];
                $secondLine = $name[1];
                break;
            case 3:
                $firstLine = "{$name[0]} {$name[1]}";
                $secondLine = $name[2];
                break;
            case 4:
                $firstLine = "{$name[0]} {$name[1]}";
                $secondLine = "{$name[2]} {$name[3]}";
                break;
            default:
                $firstLine = "{$name[0]} {$name[1]} {$name[2]}";
                $secondLine = "{$name[3]} {$name[4]}";
                break;
        }

        return [$firstLine, $secondLine];
    }

    /**
     * @param string $nameFirstLine
     * @param string $nameSecondLine
     * @return int
     */
    protected function calculateNameFontSize(string $nameFirstLine, string $nameSecondLine, int $maxFontSize) : int
    {
        $totalChars = (strlen($nameFirstLine) > strlen($nameSecondLine)) ? strlen($nameFirstLine) : strlen($nameSecondLine);

        if ($totalChars > static::MAX_NAME_CHARACTERS)
        {
            return $maxFontSize - (($totalChars - static::MAX_NAME_CHARACTERS) * 2.7);
        }

        return $maxFontSize;
    }

    /**
     * @param string $company
     * @param int $maxFontSize
     * @return int
     */
    protected function calculateCompanyFontSize(string $company, int $maxFontSize) : int
    {
        $totalChars = strlen($company);

        if ($totalChars > static::MAX_COMPANY_CHARACTERS) {
            return $maxFontSize - (($totalChars - static::MAX_COMPANY_CHARACTERS) * 1.4);
        }

        return $maxFontSize;
    }
}
