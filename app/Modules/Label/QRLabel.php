<?php


namespace App\Modules\Label;

use App\Models\Member;

use QrCode;

/**
 * Class QRLabel
 * @package App\Modules\Label
 */
class QRLabel extends LabelGenerator
{
    const NAME = 'Basic + vCard';

    const MAX_NAME_FONT_SIZE = 70;
    const MAX_NAME_CHARACTERS = 16;
    const MAX_COMPANY_FONT_SIZE = 50;
    const MAX_COMPANY_CHARACTERS = 24;

    const NAME_FIRST_LINE_POSITION_Y = 100;
    const COMPANY_POSITION_Y = 260;

    const TEXT_HORIZONTAL_CENTER_ALIGN = 342;

    /**
     * @param Member $member
     * @return \Intervention\Image\Image|void
     */
    public function make(Member $member)
    {
        $this->putName($member->name);
        $this->putCompany($member->company);
        $this->putVCard($member);
        $this->save($member);
        return $this->canvas;
    }

    /**
     * @param Member $member
     */
    protected function putVCard(Member $member) : void
    {
        $qrString = $this->makeVCardString($member);
        $qr = QRCode::format('png')->size(self::CANVAS_HEIGHT)->margin(0)->merge('/public/logo.png', .2)->generate($qrString);
        $this->canvas->insert($qr, 'right');
    }

    /**
     * @param Member $member
     * @return string
     */
    private function makeVCardString(Member $member) : string
    {
        $qrString = 'BEGIN:VCARD' . PHP_EOL;
        $qrString .= 'VERSION:3.0' . PHP_EOL;
        $qrString .= "N:{$member->name}" . PHP_EOL;
        $qrString .= "ORG:{$member->company}" . PHP_EOL;
        $qrString .= "EMAIL;TYPE=INTERNET:{$member->email}" . PHP_EOL;
        $qrString .= "TEL:{$member->phone}" . PHP_EOL;
        $qrString .= 'URL:http://www.kichile.cl' . PHP_EOL;
        $qrString .= 'END:VCARD';
        return $qrString;
    }

}
