<?php


namespace App\Modules\Label;

use App\Models\Member;

/**
 * Class BasicLabel
 * @package App\Modules\Label
 */
class BasicLabel extends LabelGenerator
{
    const NAME = 'Basic';

    const MAX_NAME_FONT_SIZE = 80;
    const MAX_NAME_CHARACTERS = 20;
    const MAX_COMPANY_FONT_SIZE = 65;
    const MAX_COMPANY_CHARACTERS = 30;

    const NAME_FIRST_LINE_POSITION_Y = 100;
    const COMPANY_POSITION_Y = 270;

    const TEXT_HORIZONTAL_CENTER_ALIGN = 495.5;

    /**
     * @param Member $member
     */
    public function make(Member $member)
    {
        $this->putName($member->name);
        $this->putCompany($member->company);
        $this->save($member);
        return $this->canvas;
    }


}
