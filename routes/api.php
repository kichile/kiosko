<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('event')->group( function() {
    Route::get('/active', 'EventController@activeEvent');
});

Route::prefix('member')->group( function() {
    Route::post('/save', 'MemberController@save');
    Route::get('/test', 'MemberController@test');
});
